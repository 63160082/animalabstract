/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.animalabstract;

/**
 *
 * @author ACER
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Bae");
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));        
        
        Animal a1 = h1;
         System.out.println("h1 is land animal ? " + (a1 instanceof LandAnimal));       
         System.out.println("h1 is reptile ? " + (a1 instanceof Reptile));
         
         System.out.println("-----------------------------------------------------------------------");
         
        Cat c1 = new Cat("Meow");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));       
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));
        System.out.println("-----------------------------------------------------------------------");
        
        Dog d1 = new Dog("Hong");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
        System.out.println("-----------------------------------------------------------------------");        
        
        Crocodile cc1 = new Crocodile("Hae");
        cc1.crawl();
        cc1.eat();
        cc1.walk();
        cc1.speak();
        cc1.sleep();
        System.out.println("cc1 is land animal ? " + (cc1 instanceof Animal));
        System.out.println("cc1 is reptile ? " + (cc1 instanceof Reptile));        
        System.out.println("-----------------------------------------------------------------------");   
        
        Snack s1 = new Snack("Soob");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is land animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile ? " + (s1 instanceof Reptile));        
        System.out.println("-----------------------------------------------------------------------");           
        
        Fish f1 = new Fish("Jom");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is aquatic animal ? " + (f1 instanceof AquaticAnimal));        
        System.out.println("-----------------------------------------------------------------------");   
        
        Crab crab1 = new Crab("Keeb");
        crab1.swim();
        crab1.eat();
        crab1.walk();
        crab1.speak();
        crab1.sleep();
        System.out.println("crab1 is aquatic animal ? " + (crab1 instanceof AquaticAnimal));     
        System.out.println("-----------------------------------------------------------------------");    
        
        Bird b1 = new Bird("Jib");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is poultry animal ? " + (b1 instanceof Poultry));     
        System.out.println("-----------------------------------------------------------------------");  

        Bat bat1 = new Bat("Black");
        bat1.fly();
        bat1.eat();
        bat1.walk();
        bat1.speak();
        bat1.sleep();
        System.out.println("bat1 is poultry animal ? " + (bat1 instanceof Poultry));             
        
    }
}
