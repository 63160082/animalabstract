/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.animalabstract;

/**
 *
 * @author ACER
 */
public class Snack extends Reptile {
    private String nickname;

    public Snack(String nickname) {
        super("Snack", 4);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Snack : " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snack : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Snack : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Snack : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snack : " + nickname + " sleep");
    }

    }

